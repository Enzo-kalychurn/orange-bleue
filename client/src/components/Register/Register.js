import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import DatePicker from "react-datepicker";
import React, { useState } from "react";
import "react-datepicker/dist/react-datepicker.css";
import './Register.css';

const Register = () => {
    const [startDate, setStartDate] = useState(new Date());
    const submitForm = async (e) => {
        e.preventDefault();
    }
    return (
        <>
            <h1><center>Inscription</center></h1>
            <Row className="mb-3"></Row>
            <Form onSubmit={submitForm}>
                <Row className="mb-3">
                    <Form.Group className="mb-3" as={Col} controlId="formBasicPrenom">
                        <Form.Label>Prénom</Form.Label>
                        <Form.Control style={{ borderColor: "blue" }} type="prenom" placeholder="Entrez votre prénom" />
                    </Form.Group>

                    <Form.Group className="mb-3" as={Col} controlId="formBasicNom">
                        <Form.Label>Nom</Form.Label>
                        <Form.Control style={{ borderColor: "blue" }} type="nom" placeholder="Entrez votre nom" />
                    </Form.Group>
                </Row>

                <Row className="mb-3">
                    <Form.Group className="mb-3" as={Col} controlId="formBasicEmail">
                        <Form.Label>Adresse e-mail</Form.Label>
                        <Form.Control style={{ borderColor: "blue" }} type="email" placeholder="Entrez votre e-mail" />
                        <Form.Text className="text-muted">
                            Nous ne partagerons jamais votre adresse e-mail.
                        </Form.Text>
                    </Form.Group>

                    <Form.Group className="mb-3" as={Col} controlId="formBasicPassword">
                        <Form.Label>Mot de passe</Form.Label>
                        <Form.Control style={{ borderColor: "blue" }} type="password" placeholder="Entrez votre mot de passe" />
                    </Form.Group>
                </Row>
                <Form.Group className="mb-3" controlId="formGridAdresse">
                    <Form.Label>Adresse</Form.Label>
                    <Form.Control style={{ borderColor: "blue" }} placeholder="Entrez votre adresse" />
                </Form.Group>

                <Row className="mb-3">
                    <Form.Group as={Col} controlId="formGridVille">
                        <Form.Label>Ville</Form.Label>
                        <Form.Control style={{ borderColor: "blue" }} />
                    </Form.Group>

                    <Form.Group as={Col} controlId="formGridPays">
                        <Form.Label>Pays</Form.Label>
                        <Form.Select style={{ borderColor: "blue" }} defaultValue="Choisir">
                            <option>France</option>
                            <option>Espagne</option>
                            <option>Suisse</option>
                            <option>Luxembourg</option>
                            <option>Italie</option>
                            <option>Belgique</option>
                            <option>Allemagne</option>
                        </Form.Select>
                    </Form.Group>

                    <Form.Group as={Col} controlId="formGridZip">
                        <Form.Label>Code postal</Form.Label>
                        <Form.Control style={{ borderColor: "blue" }} />
                    </Form.Group>
                </Row>

                <Row className="mb-3">
                    <Form.Label>Date de naissance</Form.Label>
                    <DatePicker selected={startDate} onChange={(date) => setStartDate(date)} />
                    <div className="mb-3"></div>
                </Row>
                <Row className="mb-3">

                    <Form.Select className="mb-3" as={Col} style={{ borderColor: "blue" }} aria-label="Gender">
                        <option>Séléctionnez votre genre</option>
                        <option value="homme">Homme</option>
                        <option value="femme">Femme</option>
                        <option value="autre">Autres</option>
                    </Form.Select>

                    <Form.Group className="mb-3" as={Col} controlId="formGridTel">
                        <Form.Label>Téléphone</Form.Label>
                        <Form.Control style={{ borderColor: "blue" }} />
                    </Form.Group>
                </Row>

                <div className="mb-3"></div>
                <Row className="mb-3">
                    <Form.Check label="Acceptez vous les conditions d'utilisation ?" aria-label="Oui" />
                </Row>

                <div className="mb-3"></div>
                <Row className="mb-3">
                    <Form.Check label="Je souhaite recevoir les dernières actualités des partenaires ?" aria-label="Oui" />
                </Row>

                <center><Button variant="primary" type="submit">
                    <center>Valider</center>
                </Button></center>
            </Form>
        </>
    )
}

export default Register;