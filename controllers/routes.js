const express   = require('express');
let router      = express.Router();
const Users    = require('../models/user');


// Login
router.get('/login', async(req,res) => {
    res.render('login')
})

router.post("/login", async(req, res) => {
    const userDB = await Users.findOne({email: req.body.email})
    const userDBPassword = ''
    if (userDB) {
        userDBPassword = userDB.password
    }
    if (userDBPassword === req.body.password) {
        return res.redirect('/')
    }
    res.redirect('/login')
    
})



// crud user
router.get('/user',  async(req, res) => {
    const user = Users.findById(req.params.id)
    res.json(user)

})

router.post('/register', async (req, res) => {
    const user = new Users({
        gender: req.body.title,
        lastName: req.body.lastName,
        firstName: req.body.firstName,
        email: req.body.email,
        yearOfBirth: req.body.yearOfBirth,
        address: req.body.address,
        city: req.body.city,
        postalCode: req.body.postalCode,
        country: req.body.country,
        password: req.body.password,
        emailing: req.body.emailing,
        phoneNumber: req.body.phoneNumber,
        generalCondition: req.body.generalCondition

    })
    await user.save()
    res.json(user)
})

router.delete('/user', async(req,res) => {
    //const email = req.body.email
    await Users.deleteOne({_id: req.params.id})
    res.redirect('/')
})

router.put('/user', async(req, res) => {
    await Users.updateOne(
        {_id: req.params.id},
        {
        gender: req.body.gender,
        lastName: req.body.lastName,
        firstNmae: req.body.firstName,
        email: req.body.email,
        yearOfBirth: req.body.yearOfBirth,
        address: req.body.address,
        city: req.body.city,
        postalCode: req.body.postalCode,
        country: req.body.country,
        password: req.body.password,
        emailing: req.body.emailing,
        phoneNumber: req.body.phoneNumber,
        generalCondition: req.body.generalCondition
 

     })
        res.sendStatus(204)
})


