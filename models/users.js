
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mySchema = new Schema({
    gender: { type: String, required: true },
    lastName: {type: String, required: true},
    firstName: {type: String, required: true},
    email: { type: String, required: true },
    yearOfBirth: {type: Date, required: true},
    address: { type: String, required: true },
    city: { type: String, required: true },
    postalCode: { type: int, required: true },
    country: { type: String, required: true },
    password: { type: String, required: true },
    emailing: {type: Boolean, required: true},
    phoneNumber: {type: int, required: true},
    generalCondition: {type: Boolean, required: true}
}) 
let users = mongoose.model("users", mySchema);
module.exports = users;
